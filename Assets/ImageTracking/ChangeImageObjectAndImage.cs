using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ChangeImageObjectAndImage : MonoBehaviour
{
    public ARTrackedImageManager ARImageManager;
    public List<GameObject> DisplayObject;
    public List<XRReferenceImageLibrary> ImageLibrary;
    public TMP_Dropdown menu;
    public void OnValueChange()
    {
        ARImageManager.referenceLibrary = ImageLibrary[menu.value];
        ARImageManager.trackedImagePrefab = DisplayObject[menu.value];
    }
}
